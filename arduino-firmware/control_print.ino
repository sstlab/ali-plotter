byte PWM_PIN = 7;
const int ledPin = 13;
const int average = 1150;
int pwm_value;

void setup() {
  pinMode(PWM_PIN, INPUT);
  pinMode(ledPin, OUTPUT);
  //Serial.begin(9600);
}


void loop() {
  pwm_value = pulseIn(PWM_PIN, HIGH);

  if (pwm_value > average) {
    digitalWrite(ledPin, LOW);  //開啟
  }
  else {
    digitalWrite(ledPin, HIGH); //關閉
  }

  //Serial.println(pwm_value);
}
